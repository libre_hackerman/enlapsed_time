/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <enlapsed_time.h>

static void help_message()
{
	int opt_width = 29;
	printf("Usage: %s [-i|-c] [OPTIONS] [ID]\n\n", PROG_NAME);
	printf("%s is a program to measure the time between an \"init\"", PROG_NAME);
	printf(" call and a \"comp\" call. It will display the time in hours,");
	printf(" minutes, seconds and hundredths.\n\nOptions:\n");
	printf("%-*s  Sets init mode\n", opt_width, "-i, --init");
	printf("%-*s  Sets comp mode\n", opt_width, "-c, --comp");
	printf("%-*s  Sets automatic ID based on the tty\n",
			opt_width, "-a, --autoid");
	printf("%-*s  Displays all the time units, despite being 0\n",
			opt_width, "-A, --all");
	printf("%-*s  Sets the highest time unit to display (compat. with -A)\n",
			opt_width, "-M <unit>, --max <unit>");
	printf("%-*s  Sets the lowest time unit to display (compat. with -A)\n",
			opt_width, "-m <unit>, --min <unit>");
	printf("%-*s  Sets the separator between time units (Def: ':')\n",
			opt_width, "-s <char>, --separator <char>");
	printf("%-*s  Do not set time measured to 0 after comp\n",
			opt_width, "-k, --keep");
	printf("%-*s  Displays this help\n",
			opt_width, "-h, --help");
}

static enum e_time_unit parse_unit(char *arg, t_options *ops)
{
	if (strcmp("hours", arg) == 0)
		return (HOURS);
	else if (strcmp("minutes", arg) == 0)
		return (MINUTES);
	else if (strcmp("seconds", arg) == 0)
		return (SECONDS);
	else if (strcmp("hundredths", arg) == 0)
		return (HUNDREDTHS);
	else
	{
		fprintf(stderr, "%s is not a valid time unit\n", arg);
		free_t_options(ops);
		exit(EXIT_FAILURE);
	}
}

static t_options *new_t_options()
{
	t_options *ops = malloc(sizeof(t_options));

	ops->mode = UNSEL;
	ops->maximum_unit = HOURS;
	ops->minimum_unit = HUNDREDTHS;
	ops->separator = ':';
	ops->all_units = false;
	ops->keep_timestamps = false;
	ops->id = NULL;

	return (ops);
}

void free_t_options(t_options *ops)
{
	if (ops)
	{
		if (ops->id)
			free(ops->id);
		free(ops);
	}
}

t_options *parse_options(int argc, char *argv[])
{
	int option;
	bool autoid;
	t_options *ops;

	// Display help if no arguments
	if (argc == 1)
	{
		help_message();
		exit(EXIT_SUCCESS);
	}

	autoid = false;
	ops = new_t_options();

	struct option long_options[] = {
		{"init", no_argument, NULL, 'i'},
		{"comp", no_argument, NULL, 'c'},
		{"autoid", no_argument, NULL, 'a'},
		{"all", no_argument, NULL, 'A'},
		{"max", required_argument, NULL, 'M'},
		{"min", required_argument, NULL, 'm'},
		{"separator", required_argument, NULL, 's'},
		{"keep", no_argument, NULL, 'k'},
		{"help", no_argument, NULL, 'h'},
		{0, 0, 0, 0}
	};
	while ((option = getopt_long(argc, argv, ":icaAM:m:s:kh",
					long_options, NULL)) != -1)
	{
		switch (option)
		{
			case 'i':
				ops->mode = INIT;
				break;
			case 'c':
				ops->mode = COMP;
				break;
			case 'a':
				autoid = true;
				ops->id = retrieve_tty();
				break;
			case 'A':
				ops->all_units = true;
				break;
			case 'M':
				ops->maximum_unit = parse_unit(optarg, ops);
				break;
			case 'm':
				ops->minimum_unit = parse_unit(optarg, ops);
				break;
			case 's':
				ops->separator = *optarg;
				break;
			case 'k':
				ops->keep_timestamps = true;
				break;
			case 'h':
				help_message();
				free_t_options(ops);
				exit(EXIT_SUCCESS);
			case ':':
				fprintf(stderr, "Option needs an argument\n");
				free_t_options(ops);
				exit(EXIT_FAILURE);
			case '?':
				fprintf(stderr, "Option not recognized\n");
				free_t_options(ops);
				exit(EXIT_FAILURE);
		}
	}

	if (!autoid)
	{
		if (optind == argc)  // No ID provided
		{
			fprintf(stderr, "%s requires an ID for the timestamp\n", PROG_NAME);
			free_t_options(ops);
			exit(EXIT_FAILURE);
		}
		ops->id = strdup(argv[optind]);  // To allow free, because of autoid
		if (ops->mode == UNSEL)
		{
			fprintf(stderr, "Please specify init (-i) or comparison (-c) mode\n");
			free_t_options(ops);
			exit(EXIT_FAILURE);
		}
	}

	return (ops);
}

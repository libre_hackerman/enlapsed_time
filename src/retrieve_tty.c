/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <unistd.h>
#include <enlapsed_time.h>

/*
 * This function searches and replaces all the occurrences of a character
 * in a string, returning the new string allocated in the heap.
 */
static char* search_and_replace(char* str, char c_old, char c_new)
{
	char *dup = strdup(str);
	int len = strlen(dup);

	for (int i = 0; i < len; i++)
	{
		if (dup[i] == c_old)
			dup[i] = c_new;
	}
	return (dup);
}

/*
 * This functions returns an string in the heap specifying the TTY from
 * where the program was called, in a syntax like ",dev,tty1", substituting
 * the / for , so it can be used in a filename.
 */
char* retrieve_tty()
{
	return (search_and_replace(ttyname(STDIN_FILENO), '/', ','));
}

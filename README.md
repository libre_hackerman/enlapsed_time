# enlapsed_time

### What's this about
This program is used to measure the time interval between two calls to the program, displaying to stdout the time interval in the format of `hours:minutes:seconds:hundredths`, but displaying only the necessary fields (ej, 73.2s is presented as 1:13:20, omitting hours).

Example of use:
```
$ enlapsed_time --init ex && sleep 73.2 && enlapsed_time --comp ex
1:13:30
```

In this example, `ex` is the ID of the timestamp, so multiple instancies of enlapsed_time can be run at the same time as long as they have different IDs.
Of course, this example is not specially useful, since the command `time` is a thing. The reason I wrote this program was to get a fast and fancy display of the commands duration in my shell prompt. A simple example of this put in the `.bashrc`:
```
PS0='$(enlapsed_time --init --autoid)'
PS1='$(enlapsed_time --comp --autoid) \$ '
```
Would produce a prompt like this:
```
00 $ sleep 12.37
12:37 $ sleep 2
2:00 $
```
The `--autoid` option gets automatically an ID for the terminal session. The PS0 is displayed *before* the command starts. With init, the program won't display anything so it doesn't disturb. And in the prompt (PS1), we can place an expansion to get the time difference.

#### Options
The options list can be displayed with `-h` or `--help`:
```
Usage: enlapsed_time [-i|-c] [OPTIONS] [ID]

enlapsed_time is a program to measure the time between an "init" call and a "comp" call. 
It will display the time in hours, minutes, seconds and hundredths.

Options:
-i, --init                     Sets init mode
-c, --comp                     Sets comp mode
-a, --autoid                   Sets automatic ID based on the tty
-A, --all                      Displays all the time units, despite being 0
-M <unit>, --max <unit>        Sets the highest time unit to display (compat. with -A)
-m <unit>, --min <unit>        Sets the lowest time unit to display (compat. with -A)
-s <char>, --separator <char>  Sets the separator between time units (Def: ':')
-k, --keep                     Do not set time measured to 0 after comp
-h, --help                     Displays this help
```

### Installation

#### Dependencies
* Make
* C compiler (GCC, clang)

In order to simply install enlapsed_time, type this commands on a shell:
```
$ git clone https://gitlab.com/libre-hackerman/enlapsed_time
$ cd enlapsed_time
$ make
$ sudo make install
```
The timestamps are stored by default in `/tmp/terms_timestamps`. You can change that folder editing the Makefile.

### Uninstallation
To remove the installed binary, being in the repository folder:
```
sudo make uninstall
```
